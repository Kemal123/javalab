package ru.itis;


import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.itis.models.Course;
import ru.itis.models.Student;
import ru.itis.models.Teacher;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CoursesRepositoryImpl implements CoursesRepository {
    private final JdbcTemplate jdbcTemplate;

    public CoursesRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate =  new JdbcTemplate(dataSource);
    }

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select *, k.id as course_id from ((course c left join course_student cs on c.id = cs.id_course) k inner join student s on k.id_student = s.id) where id_course = ?";
    //language=SQL
    private static final String SQL_INSERT = "insert into course (name, date_start, date_end, id_teacher) values (?, ?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select *, k.id as course_id from ((course c left join course_student cs on c.id = cs.id_course) k inner join student s on k.id_student = s.id) where id_course = ?";


    private final RowMapper<Course> courseRowMapper = new RowMapper<Course>() {
        @Override
        public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
            Course course = null;

            try {
                Integer id = rs.getInt("id");
                String name = rs.getString("name");
                String dateStart= rs.getString("date_start");
                String dateEnd = rs.getString("data_end");
                Teacher teacher = new Teacher(rs.getInt("id_teacher"));

                course = new Course(id, name, dateStart, dateEnd, teacher, new ArrayList<>());
            } catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }

            return course;
        }
    };

    private final RowMapper<Student> studentRowMapper = new RowMapper<Student>() {
        @Override
        public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
            Student student = new Student(rs.getInt("id_student"),
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getInt("group_s"));

            return student;
        }
    };

    private final ResultSetExtractor<List<Course>> courseListResultSetExtractor = new ResultSetExtractor<List<Course>>() {
        @Override
        public List<Course> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<Course> courses = new ArrayList<>();
            Set<Integer> integerSet = new HashSet<>();
            Course currentCourse = null;

            while(rs.next()) {
                if(!integerSet.contains(rs.getInt("id_course"))) {
                    currentCourse = courseRowMapper.mapRow(rs, rs.getRow());
                    courses.add(currentCourse);
                }

                if(rs.getObject("id_student") != null) {
                    Student student = studentRowMapper.mapRow(rs, rs.getRow());
                    currentCourse.getStudents().add(student);
                }

                integerSet.add(currentCourse.getId());
            }

            return courses;
        }
    };



    @Override
    public List<Course> findByName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_BY_NAME, courseListResultSetExtractor, searchName);
    }

    @Override
    public void save(Course course) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});

            preparedStatement.setString(1, course.getName());
            preparedStatement.setString(2, course.getDateStart());
            preparedStatement.setString(3, course.getDateEnd());
            preparedStatement.setInt(4, course.getTeacher().getId());

            return preparedStatement;
        }, keyHolder);

        course.setId(keyHolder.getKey().intValue());
    }


    @Override
    public Optional<Course> findById(Integer id) {
        return jdbcTemplate.query(SQL_SELECT_BY_ID, courseListResultSetExtractor, id).stream().findAny();
    }


}
