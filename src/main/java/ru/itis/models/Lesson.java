package ru.itis.models;

public class Lesson {
    private Integer idLesson;
    private String nameLesson;
    private String time;
    private Course course;



    public Integer getIdLesson() {
        return idLesson;
    }

    public void setIdLesson(Integer idLesson) {
        this.idLesson = idLesson;
    }

    public String getNameLesson() {
        return nameLesson;
    }

    public void setNameLesson(String nameLesson) {
        this.nameLesson = nameLesson;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "idLesson=" + idLesson +
                ", nameLesson='" + nameLesson + '\'' +
                ", time='" + time + '\'' +
                ", course=" + course +
                '}';
    }
}
