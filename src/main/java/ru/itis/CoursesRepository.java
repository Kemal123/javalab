package ru.itis;



import ru.itis.models.Course;

import java.util.List;
import java.util.Optional;

public interface CoursesRepository {
    List<Course> findByName(String searchName);
    void save(Course course);
    Optional<Course> findById(Integer id);
}
